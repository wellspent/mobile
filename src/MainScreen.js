import React from 'react';
import {
  Button,
  SectionList,
  StyleSheet,
  Text,
  TextInput,
  View,
  Picker,
  TouchableNativeFeedback,
  TouchableOpacity,

} from 'react-native';
import moment from 'moment';
import {connect} from 'react-redux';
import {actionCreator} from "./state";
import {uuid4} from "./utils";

function SummaryParseError(message) {
  this.message = message;
}

function AmountParseError(message) {
  this.message = message;
}

function TagsParseError(message) {
  this.meesage = message;
}

class Entry {
  constructor(summary, amount, tags) {
    this.id = uuid4();
    this.ts = new Date();
    this.summary = this.validateSummary(summary);
    this.amount = this.validateAmount(amount);
    this.tags = this.validateTags(tags);
  }
  validateSummary = (value) => {
    if (!value) {
      throw new SummaryParseError("Provide summary");
    }
    const summary = value.trim();
    if (summary === '') {
      throw new SummaryParseError("Provide summary");
    }
    return summary;
  };
  validateAmount = (value) => {
    if (!value) {
      throw new AmountParseError("Amount is empty")
    }

    const numericValue = Number.parseFloat(value);
    if (Number.isNaN(numericValue)) {
      throw new AmountParseError("Amount must be decimal number")
    }

    return numericValue;
  };
  validateTags = (value) => {
    if (!value) {
      throw new TagsParseError("Tags not provided")
    }
    const tagsList = value.match(/\S+/g) || [];
    if (tagsList.length === 0) {
      throw new TagsParseError("Whitespaces are not valid tags")
    }
    return tagsList;
  };
}

class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      summary: null,
      amount: null,
      tags: null,
      collapsedPeriods: [],
      isAddingEntry: false
    };
    this.summaryRef = React.createRef();
    this.amountRef = React.createRef();
    this.tagsRef = React.createRef();
  }

  // UI handlers
  onAddButtonPress = () => {
    if (!this.state.isAddingEntry) {
      this.setState({isAddingEntry: true});
      return;
    }
    const summaryInput = this.state.summary;
    const amountInput = this.state.amount;
    const tagsInput = this.state.tags;
    try {
      const entry = new Entry(summaryInput, amountInput, tagsInput);
      // todo: ui fields and state are separate. Better draw values directly from UI input fields (don't know how, refs didn't work)
      this.props.addExpense(entry);
      this.setState({
        summary: null,
        amount: null,
        tags: null,
        isAddingEntry: false,
      });
      this.summaryRef.current.clear();
      this.amountRef.current.clear();
      this.tagsRef.current.clear();
    }
    catch (error) {
      console.warn(error)
    }
  };
  onUnfoldAllButtonPress = (sections) => {
    const current = sections.map((value) => value.ts);
    this.setState({
      collapsedPeriods: this.state.collapsedPeriods.filter((element) => !current.includes(element))
    })
  };
  onCollapseAllButtonPress = (sections) => {
    const collapsed = this.state.collapsedPeriods;
    this.setState({
      collapsedPeriods: [...collapsed, ...sections.map((value) => value.ts)],
    })
  };
  onPeriodPress = (triggerCollapse) => {
    const collapsedPeriods = this.state.collapsedPeriods;
    const pos = collapsedPeriods.findIndex((element) => element === triggerCollapse);
    if (pos === -1) {
      this.setState({collapsedPeriods: [triggerCollapse, ...collapsedPeriods]})
    }
    else {
      this.setState({collapsedPeriods: collapsedPeriods.filter((element, index) => index !== pos)})
    }
  };
  prepareList = (groupBy, data) => {
    /*
    Transform data of the shape:
      [
        { ts, summary, amount, ... },
        { ts, summary, amount, ... },
        ...
      ]
     to grouped by week data:
    [
      { ts: '2018-W52', amount, data: [
        { ts, summary, amount },
        { ts, summary, amount },
      ]},
      { ts: '2018-W51', ... },
      { ts: '2018-W50 ', ... },
      ...
    ]
     */
    data.sort((a, b) => new Date(a.ts) - new Date(b.ts));
    /*
    {
      "2018-W52": {
        amount: number,
        data: [
          { ts, summary, amount },
          { ts, summary, amount },
        ]
      },
      "2018-W51": [
        { ts, summary, amount },
        { ts, summary, amount },
      ],
    }
     */
    const buckets = {};
    for (let entry of data) {
      let format = null;
      switch (groupBy) {
        case 'weeks':
          format = 'YYYY-[W]WW';
          break;
        case 'days':
          format = 'YYYY-MM-DD';
          break;
        case 'months':
          format = 'YYYY-MM';
      }
      let period = moment(entry.ts).format(format);  // ISO 8601: 2018-W52
      let content = buckets[period];
      if (!content) {
        content = {
          amount: entry.amount,
          data: [entry]
        }
      }
      else {
        content.amount += entry.amount;
        content.data.push(entry);
      }
      buckets[period] = content;
    }

    const sortedPeriods = Object.keys(buckets).sort((a, b) => moment(b) - moment(a));
    const sections = [];
    for (let period of sortedPeriods) {
      let periodData = buckets[period];
      sections.push({
        ts: period,
        amount: periodData.amount,
        data: periodData.data,
        collapsed: this.state.collapsedPeriods.includes(period)})
    }
    return sections;
  };
  _formatPeriod = (ts, period) => {
    switch (period) {
      case 'days': {
        const formats = {
          sameDay: '[Today]',
          nextDay: '[Tomorrow]',
          nextWeek: 'dddd',
          lastDay: '[Yesterday]',
          lastWeek: '[Last] dddd',
          sameElse: 'DD/MM/YYYY'
        };
        return moment(ts).calendar(null, formats);
      }
      case 'weeks': {
        const moment_ts = moment(ts);
        const weekNumber = moment_ts.week();
        const thisWeek = moment().week();
        switch (weekNumber) {
          case thisWeek:
            return 'This week';
          case thisWeek -1:
            return 'Last week';
          default:
            let start = moment_ts.date();
            let month = moment_ts.format('MMMM');
            let end = moment_ts.isoWeekday(7).date();
            return `${month}, ${start}-${end}`
        }
      }
      case 'months': {
        return moment(ts).format('MMMM');
      }
      default:
        throw 'Unsupported period ' + period
    }
  };
  renderSectionHeader = ({section}) => {
    // todo: this gets called twice, don't know why
    return (
        <TouchableNativeFeedback onPress={() => this.onPeriodPress(section.ts)}>
          <View style={styles.sectionHeaderView}>
            <Text style={styles.sectionHeaderText}>{this._formatPeriod(section.ts, this.props.period)}</Text>
            <Text style={styles.sectionHeaderAmountText}>{section.amount}</Text>
          </View>
        </TouchableNativeFeedback>
    )
  };
  renderItem = ({item, index, section}) => {
    // if (this.state.collapsedPeriods.includes(section.ts)) {
    if (section.collapsed) {
      return null;
    }

    return (
        <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Details', {id: item.id})}
        >
          <View style={styles.sectionItemView}>
            <Text style={styles.sectionItemText}>{`${item.summary}: ${item.amount} `}</Text>
          </View>
        </TouchableOpacity>
    )
  };
  // !UI handlers
  render() {
    // todo: on each .setState this gets called, optimize?
    const sections = this.prepareList(this.props.period, this.props.data);
    const showInputs = this.state.isAddingEntry;
    return (
        <View style={styles.container}>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>Where's My Money</Text>
          </View>
          <View style={styles.addEntryView}>
            {
              showInputs &&
                  <View style={styles.addEntryInputsView}>
                    <TextInput
                      style={styles.summaryTextInput}
                      placeholder='Summary'
                      ref={this.summaryRef}
                      onChangeText={(text) => this.setState({summary: text})}
                      onSubmitEditing={() => this.amountRef.current.focus()}
                      />
                    <TextInput
                      style={styles.amountTextInput}
                      placeholder='Amount'
                      keyboardType='decimal-pad'
                      ref={this.amountRef}
                      onChangeText={(text) => this.setState({amount: text})}
                      onSubmitEditing={() => this.tagsRef.current.focus()}
                      />
                    <TextInput
                      style={styles.tagsTextInput}
                      placeholder='Tags'
                      ref={this.tagsRef}
                      onChangeText={(text) => this.setState({tags: text})}
                      />
                  </View>
            }
            <View style={styles.addEntryButtonsView}>
            <Button
              style={styles.addButton}
              title='add'
              onPress={this.onAddButtonPress}
              />
            {
              showInputs &&
                  <Button
                    title='cancel'
                    onPress={() => this.setState({isAddingEntry: false})}
                    />
            }
            </View>
          </View>
          <View style={styles.browseEntriesView}>
            <View style={styles.toolbarView}>
              <Button
                  style={styles.collapseAllButton}
                  title='-'
                  onPress={() => this.onCollapseAllButtonPress(sections)}
              />
              <Picker
                  style={styles.periodPicker}
                  selectedValue={this.props.period}
                  onValueChange={(itemValue, itemIndex) => this.props.changePeriod(itemValue)}>
                <Picker.Item label="Days" value="days" />
                <Picker.Item label="Weeks" value="weeks" />
                <Picker.Item label="Months" value="months" />
              </Picker>
              <Button
                  style={styles.unfoldAllButton}
                  title='+'
                  onPress={() => this.onUnfoldAllButtonPress(sections)}
              />
            </View>
            <SectionList
              sections={sections}
              renderSectionHeader={this.renderSectionHeader}
              stickySectionHeadersEnabled={true}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => item.id}
              />
          </View>

          <Button title='log state' onPress={() => console.log(this.state)}/>
          {/*<Button title='log data' onPress={() => console.log(this.props.data)}/>*/}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titleView: {
  },
  titleText: {
    fontSize: 24,
    paddingTop: 24,
    padding: 12,
  },
  addEntryView: {},
  addEntryInputsView: {},
  summaryTextInput: {
    padding: 12,
  },
  amountTextInput: {
    padding: 12,
  },
  tagsTextInput: {
    padding: 12,
  },
  addEntryButtonsView: {},
  addButton: {},
  browseEntriesView: {
    flex: 1
  },
  sectionHeaderView: {
    backgroundColor: "#effcff",
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  sectionHeaderText: {
    padding: 12,
    fontSize: 24,
  },
  sectionHeaderAmountText: {
    padding: 12,
    fontSize: 24,
  },
  sectionItemView: {},
  sectionItemText: {
    padding: 16,
  },
  toolbarView: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  collapseAllButton: {
    // todo: how to adjust button's width (relative) with flexDirection: 'row'?
    flex: 1,
  },
  unfoldAllButton: {
    flex: 1,
  },
  periodPicker: {
    flex: 0.5,
  }
});

const mapStateToProps = (state) => {
  return {
    data: state.data,
    period: state.period
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addExpense: expenseEntry => {
      dispatch(actionCreator.addExpenseItem(expenseEntry))
    },
    changePeriod: period => {
      dispatch(actionCreator.changePeriod(period))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
