import React from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  DatePickerAndroid
} from 'react-native';
import {connect} from 'react-redux';
import moment from "moment";
import {actionCreator} from "./state";

class DetailsScreen extends React.Component {
  constructor(props) {
    super(props);
    const id = this.props.navigation.getParam('id', null);
    this.originalEntry = this.props.data.find((element) => element.id === id);
    this.state = {
      // todo: probably need to deep copy here
      entry: {...this.originalEntry},
      isEntryChanged: false,
    };
  }
  onDateFieldPress = () => {
    const initialTs = moment(this.state.entry.ts);
    const ts = moment(initialTs);
    const date = new Date(ts.year(), ts.month(), ts.date());
    // todo: date picker iOS support
    DatePickerAndroid.open({date: date})
        .then(({action, year, month, day}) => {
          if (action !== DatePickerAndroid.dismissedAction) {
            ts.set({year: year, month: month, date: day});

            if (ts !== initialTs) {
              this.setState({
                entry: {...this.state.entry, ts: ts},
                isEntryChanged: ts.format('LL') !== moment(this.originalEntry.ts).format('LL'),
              });
            }
          }
    });
  };
  onSaveButtonPress = () => {
    this.props.dispatch(actionCreator.changeExpenseItem(this.state.entry));
    this.originalEntry = {...this.state.entry};
    this.setState({isEntryChanged: false});
  };
  onRemoveButtonPress = () => {
    this.props.dispatch(actionCreator.removeExpenseItem(this.originalEntry.id));
    this.props.navigation.navigate('Home');
  };
  render() {
    return (
        <View>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>Details</Text>
          </View>
          <View style={styles.detailsView}>
            <TouchableOpacity onPress={() => this.onDateFieldPress()}>
              <View style={styles.detailsFieldView}>
                <Text>Date:</Text>
                <Text>{moment(this.state.entry.ts).format("LL")}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.detailsButtonsView}>
            <Button
                style={styles.detailsSaveButton}
                title='save'
                disabled={!this.state.isEntryChanged}
                onPress={() => this.onSaveButtonPress()}
            />
            <Button
                style={styles.detailsRemoveButton}
                title='remove'
                color='red'
                onPress={() => this.onRemoveButtonPress()}
            />
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  titleView: {
  },
  titleText: {
    fontSize: 24,
    paddingTop: 24,
    padding: 12,
  },
  detailsView: {

  },
  detailsFieldView: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'

  },
  detailsButtonsView: {},
  detailsSaveButton: {},
  detailsRemoveButton: {},
});

const mapStateToProps = (state) => {
  return {
    data: state.data,
  }
};

export default connect(mapStateToProps)(DetailsScreen);
