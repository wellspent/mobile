import {createStackNavigator, createAppContainer} from 'react-navigation';
import MainScreen from "./MainScreen";
import DetailsScreen from "./DetailsScreen";

const stack = createStackNavigator({
  "Home": {
    screen: MainScreen,
  },
  "Details": {
    screen: DetailsScreen,
  },

});

export default createAppContainer(stack);