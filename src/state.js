import {uuid4} from "./utils";

let exampleData = [
  {id: uuid4(), amount: 200, summary: 'mcdonnalds', ts: new Date(2018, 11, 16)},
  {id: uuid4(), amount: 200, summary: 'mcdonnalds', ts: new Date(2018, 11, 17)},
  {id: uuid4(), amount: 200, summary: 'mcdonnalds', ts: new Date(2018, 11, 21)},
  {id: uuid4(), amount: 400, summary: 'mcdonnalds', ts: new Date(2018, 11, 22)},
  {id: uuid4(), amount: 300, summary: 'mcdonnalds', ts: new Date()},
  {id: uuid4(), amount: 100, summary: 'mcdonnalds', ts: new Date(2018, 11, 20)},
];

const initialState = {
  data: exampleData,
  // data: [],
  period: 'days',
};

const ACTION_TYPE = {
  EXPENSE_ADD_ITEM: 'EXPENSE_ADD_ITEM',
  EXPENSE_CHANGE_ITEM: 'EXPENSE_CHANGE_ITEM',
  EXPENSE_REMOVE_ITEM: 'EXPENSE_REMOVE_ITEM',
  CHANGE_PERIOD: 'CHANGE_PERIOD',
};

export const actionCreator = {
  addExpenseItem: (expenseEntry) => {
    return {
      type: ACTION_TYPE.EXPENSE_ADD_ITEM,
      expenseEntry
    }
  },
  changeExpenseItem: (expenseEntry) => {
    return {
      type: ACTION_TYPE.EXPENSE_CHANGE_ITEM,
      expenseEntry
    }
  },
  removeExpenseItem: (entryId) => {
    return {
      type: ACTION_TYPE.EXPENSE_REMOVE_ITEM,
      entryId: entryId
    }
  },
  changePeriod: (period) => {
    return {
      type: ACTION_TYPE.CHANGE_PERIOD,
      period
    }
  }
};

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case ACTION_TYPE.EXPENSE_ADD_ITEM: {
      return {
        ...state,
        data: [action.expenseEntry, ...state.data]
      }
    }
    case ACTION_TYPE.CHANGE_PERIOD: {
      return {
        ...state,
        period: action.period
      }
    }
    case ACTION_TYPE.EXPENSE_CHANGE_ITEM: {
      const data = state.data.filter((entry) => entry.id !== action.expenseEntry.id);
      return {
        ...state,
        data: [...data, action.expenseEntry]
      }
    }
    case ACTION_TYPE.EXPENSE_REMOVE_ITEM: {
      return {
        ...state,
        data: state.data.filter((entry) => entry.id !== action.entryId),
      }
    }
    default:
      return state;
  }
}
